package jd.plugins.hoster;

import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.appwork.utils.encoding.URLEncode;
import org.jdownloader.gui.InputChangedCallbackInterface;
import org.jdownloader.plugins.accounts.AccountBuilderInterface;

import jd.PluginWrapper;
import jd.plugins.Account;
import jd.plugins.AccountInfo;
import jd.plugins.BrowserAdapter;
import jd.plugins.DefaultEditAccountPanel;
import jd.plugins.DownloadLink;
import jd.plugins.DownloadLink.AvailableStatus;
import jd.plugins.HostPlugin;
import jd.plugins.LinkStatus;
import jd.plugins.PluginException;
import jd.plugins.PluginForHost;

@HostPlugin(revision = "$Revision: 3 $", interfaceVersion = 2, names = { "pr0load.com" }, urls = { "" }, flags = { 2 })
public class Pr0loadCom extends PluginForHost {

    private static final Pattern TIME2WAIT_REGEX     = Pattern.compile("(?s).*setTimeout\\s*\\(\\s*function\\(\\s*\\)\\s*\\{\\s*window\\.location\\s*=\\s*\"\\.\\.\\/\"\\s*;\\s*\\}\\s*,\\s*(\\d+)\\s*\\)\\s*;.*");
    private static final Pattern DOWNLOAD_LINK_REGEX = Pattern.compile("(?s).*<a href=\"(\\?dl=\\w+&key=\\w+)\".*>");
    private static final Pattern POST_FAILED_REGEX   = Pattern.compile("(?s).*<p\\s*id=\"message\"\\s*>\\s*<\\/p>.*");
    private static Object        staticLock          = new Object();
    private static final int     MAX_SIM_DOWNLOADS   = 5;
    private static long          timeToWait          = -1;

    private static long          lastRequestTime     = 0;
    private int                  postFailedCount     = 0;

    public Pr0loadCom(PluginWrapper wrapper) {
        super(wrapper);
        this.enablePremium("");
    }

    @Override
    public void init() {
        try {
            br.setFollowRedirects(true);
            String ddosPage = doRequest(null); // get session cookie
            br.setFollowRedirects(false);
            if ("http://pr0load.com/DDOS/".equals(br.getURL())) {
                Matcher ttwMatcher = TIME2WAIT_REGEX.matcher(ddosPage);
                if (ttwMatcher.find()) {
                    timeToWait = Long.parseLong(ttwMatcher.group(1));
                } else {
                    timeToWait = -1;
                }
            }
        } catch (IOException | PluginException | NullPointerException e) {
            logger.log(e);
            e.printStackTrace();
            timeToWait = -1;
        }
    }

    public long getTimeToWait() {
        if (timeToWait > 0) {
            return timeToWait;
        }
        return 6000l;
    }

    @Override
    public String getAGBLink() {
        return "https://pr0gramm.com/new/1089096";
    }

    @Override
    public AvailableStatus requestFileInformation(DownloadLink downloadLink) throws Exception {
        return AvailableStatus.TRUE;
    }

    @Override
    public AccountInfo fetchAccountInfo(Account account) throws Exception {
        AccountInfo ai = super.fetchAccountInfo(account);
        ai.setMultiHostSupport(this, Arrays.asList(new String[] { "uploaded.to" }));
        account.setMaxSimultanDownloads(MAX_SIM_DOWNLOADS);
        return ai;
    }

    @Override
    public AccountBuilderInterface getAccountFactory(final InputChangedCallbackInterface callback) {
        return new DefaultEditAccountPanel(callback) {
            private static final long serialVersionUID = 4059911224372903102L;

            @Override
            public boolean validateInputs() {
                return true;
            }

            @Override
            public void setAccount(Account defaultAccount) {
            }

        };
    }

    @Override
    public void handleMultiHost(DownloadLink uploadedLink, Account account) throws Exception {
        String encodedUploadedLink = URLEncode.encodeRFC2396(uploadedLink.getPluginPatternMatcher());
        String post = "file=" + encodedUploadedLink + "&submit=Starting download...";
        String pr0loadLink = null;
        for (int retryCount = 0; pr0loadLink == null; retryCount++) {
            if (retryCount >= 5) {
                throw new PluginException(LinkStatus.ERROR_HOSTER_TEMPORARILY_UNAVAILABLE, "Pr0load is currently not available", 1 * 60 * 1000l);
            }
            if (postFailedCount >= 5) {
                throw new PluginException(LinkStatus.ERROR_PLUGIN_DEFECT);
            }
            String content = doRequest(post);
            Pattern p = Pattern.compile("(?s).*<p\\s*id=\"message\"\\s*>(.*?)<\\/p>.*");
            Matcher m = p.matcher(content);
            if (m.find()) {
                logger.info(m.group(1));
            }
            if (POST_FAILED_REGEX.matcher(content).matches()) {
                postFailedCount++;
            } else {
                Matcher dlMatcher = DOWNLOAD_LINK_REGEX.matcher(content);
                if (dlMatcher.find()) {
                    pr0loadLink = "http://pr0load.com/" + dlMatcher.group(1);
                } else {
                    sleep(15 * 1000l, uploadedLink, "Retry #" + (retryCount + postFailedCount));
                }
            }
        }
        dl = BrowserAdapter.openDownload(br, uploadedLink, pr0loadLink);
        dl.startDownload();
        updateRequestTime();
    }

    private static void updateRequestTime() {
        lastRequestTime = System.currentTimeMillis();
    }

    private String doRequest(String post) throws IOException, PluginException {
        String toReturn = null;
        synchronized (staticLock) {
            long currentTime = System.currentTimeMillis();
            long timeSinceLastRequest = currentTime - lastRequestTime;
            if (timeSinceLastRequest < getTimeToWait()) {
                sleep(getTimeToWait() - timeSinceLastRequest, getDownloadLink());
            }
            toReturn = br.postPageRaw("http://pr0load.com/", post);
            updateRequestTime();
        }
        return toReturn;
    }

    @Override
    public void reset() {
        postFailedCount = 0;
    }

    @Override
    public void resetDownloadlink(DownloadLink link) {
    }

    @Override
    public void handleFree(DownloadLink link) throws Exception {
    }

}
