#!/bin/sh
dir=`dirname "$0"`

#Prüfen ob 7zip verfügbar ist
command -v 7z >/dev/null 2>&1 || { echo >&2 "Zum Installieren wird 7z benötigt. Abbruch."; exit 1; }

#JD2 Verzeichnis abfragen
instdir_default="/opt/jd2"
echo "Bitte das Verzeichnis eingeben, in welchem JDownloader 2.0 installiert ist:"
read -p "[$instdir_default] " instdir
instdir=${instdir:-$instdir_default}

#Dateien auspacken
7z x -o$instdir -x'!$PLUGINSDIR' -y $dir/setup.exe