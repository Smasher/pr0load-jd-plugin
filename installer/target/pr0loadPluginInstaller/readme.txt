JDownloader Pr0load.com Hoster Plugin by Smasher.

Zur Installation unter Windows muss die im Archiv enthaltene setup.exe ausgeführt werden. Linux- und Mac-Nutzer können sich dem sehr einfachen Shell-Skript setup.sh bedienen. Der JDownloader muss nach der Installation gegebenenfalls neu gestartet werden.

Das Plugin soll einmalig in den JD-Einstellungen aktiviert werden. Dafür navigiert man zu "Einstellungen" -> "Accountverwaltung" -> "Übersicht" und drückt auf den Button "Hinzufügen" (siehe howToPluginActivation.png). Im erschienenen Dialogfenster "pr0load.com" auswählen und auf "Speichern" klicken. Anmeldeinformationen können leer gelassen werden.

Unser Pr0load-Server hat einen DDOS-Schutz, der den Benutzer 6 Sekunden warten lässt, falls dieser zu viele Anfragen innerhalb einer kurzen Zeit stellt. Das Plugin greift diese Idee auf und verhindert, dass allgemein zwischen Requests auf Pr0load mindestens 6 Sekunden Wartezeit liegt.

Die folgende Request-Folge wird abgeschickt pro einen Download: Initialer Request und Speichern der Session-Cookie, Request mit dem Download-Link, Request der Zieldatei. Also beläuft sich die Wartezeit pro Download auf 12 bis 18 Sekunden (je nach dem, ob vor dem initialen Request irgendein anderer für eine andere Datei gesendet wurde). Die Wartezeit kann sich jedoch verlängern, falls Server nicht wie erwartet reagiert. Bekannt sind mehrere Ursachen für eine solche velängerte Wartezeit:

 * Session ID existiert nicht mehr.
 * Der Download-Volumen bei Uploaded ist im Moment komplett aufgebraucht. Probiere bitte später noch einmal.
 * Nachricht "Download nicht möglich, ist die Datei online und öffentlich?"

In diesem Fall wird 15 Sekunden gewartet. Nach dem fünften fehlgeschlagenen Versuch einen Downloadlink zu bekommen wird der jeweilige Download mit dem "echten" Hoster Plugin betätigt. Falls das nicht erwünscht ist (z.B. weil man auf keinen Fall Captchas eingeben möchte), kann das Plugin als einzig verfügbarer für uploaded.to eingestellt werden. Dazu navigiert man zu "Einstellungen" -> "Accountverwaltung" -> "Verwendungsregeln" (siehe howToAccountUsageRule.png) und fügt eine neue Regel für den Hoster "uploaded.to" hinzu. Im Dialogfenster kann dann das Uploaded-Plugin deaktiviert werden. Mit dieser Einstellung zeigt das Plugin nach dem fünften Fehlversuch eine Nachricht an, dass Pr0load momentan nicht verfügbar ist und wartet eine Minute, bevor es den Download nochmal versucht.

Ein Tipp zur selbständigen Fehlerermittlung: Im Verzeichnis C:\Users\Pr0user\AppData\Local\JDownloader v2.0\logs gibt es Log-Dateien, die nach JD-Sessions in den Ordnern untergebracht sind. Falls in der jeweiligen Session das Plugin benutzt wurde/wird, dann sollte dort mindestens eine Datei mit dem Namen "uploaded.to_jd.plugins.hoster.Uploadedto_hiddenshop.biz.log.0" vorhanden sein. Je nach dem, ob das Plugin viele Log-Informationen gesammelt hat, können dort mehrere Dateien mit Dateierweiterungen .0, .1, .2, etc vorhanden sein. In diesen Dateien können die kompletten HTTP-Requests und HTTP-Responses sowie empfangene HTML-Inhalte eingesehen werden. Daraus können die z.B. von Pr0load ausgegebene Fehlermeldungen ersehen werden.

Viel Spaß!