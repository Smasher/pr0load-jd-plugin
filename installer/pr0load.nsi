;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"

;--------------------------------
;General

  ;Name and file
  Name "JDownloader2 Pr0load.com Hoster Plugin"
  OutFile "target\pr0loadPluginInstaller\setup.exe"

  ;Default installation folder
  InstallDir "$LOCALAPPDATA\JDownloader v2.0"
  
  ;Request application privileges for Windows Vista
  RequestExecutionLevel user

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING
  !define MUI_TEXT_ABORTWARNING "M�chtest du die Installation wirklich abbrechen?"
  !define MUI_ICON "media\pr0gramm.ico"
  !define MUI_UNICON "media\pr0gramm_uninstall.ico"
  !define MUI_HEADERIMAGE
  !define MUI_HEADERIMAGE_BITMAP "media\pr0+jd+hs.bmp"
  !define MUI_HEADERIMAGE_RIGHT

;--------------------------------
;Pages
  
  !define MUI_TEXT_DIRECTORY_TITLE "JDownloader 2 Verzeichnis ausw�hlen"
  !define MUI_TEXT_DIRECTORY_SUBTITLE "Bitte das Verzeichnis ausw�hlen, in welchem JDownloader 2.0 installiert ist."
  !define MUI_DIRECTORYPAGE_TEXT_TOP "$\r$\nVielen Dank an Redf0x f�r die zur Verf�gung gestellte Ressource$\r$\n$\t und an Pr0gramm daf�r, dass es uns zusammengef�hrt hat!"
  !define MUI_DIRECTORYPAGE_TEXT_DESTINATION "JDownloader 2 Verzeichnis"

  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "German"

;--------------------------------
;Installer Sections

Section "InstallFiles"

  SetOutPath "$INSTDIR\jd\plugins\hoster"
  File "installersrc\jd\plugins\hoster\Pr0loadCom.class"
  File "installersrc\jd\plugins\hoster\Pr0loadCom$1.class"
  SetOutPath "$INSTDIR\themes\standard\org\jdownloader\images\fav"  
  File "installersrc\themes\standard\org\jdownloader\images\fav\big.pr0load.com.png"
  
  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall Pr0load Hoster Plugin.exe"

SectionEnd

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  Delete "$INSTDIR\jd\plugins\hoster\Pr0loadCom.class"
  Delete "$INSTDIR\themes\standard\org\jdownloader\images\fav\big.pr0load.com.png"  
  Delete "$INSTDIR\Uninstall Pr0load Hoster Plugin.exe"

SectionEnd