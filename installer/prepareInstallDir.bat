set classPath=..\JDownloader\bin
set installerSrcPath=installersrc

mkdir %~dp0\%installerSrcPath%\jd\plugins\hoster
mkdir %~dp0\%installerSrcPath%\themes\standard\org\jdownloader\images\fav

copy /Y %classPath%\jd\plugins\hoster\Pr0loadCom.class %~dp0\%installerSrcPath%\jd\plugins\hoster
if %errorLevel% NEQ 0 goto err

copy /Y %classPath%\jd\plugins\hoster\Pr0loadCom$1.class %~dp0\%installerSrcPath%\jd\plugins\hoster
if %errorLevel% NEQ 0 goto err

copy /Y %classPath%\themes\standard\org\jdownloader\images\fav\big.pr0load.com.png %~dp0\%installerSrcPath%\themes\standard\org\jdownloader\images\fav
if %errorLevel% NEQ 0 goto err

exit /b 0

:err
pause
exit /b %errorLevel%